// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "Defector_StateEffect.generated.h"

/**
 * 
 */
UCLASS(Blueprintable, BlueprintType)
class DEFECTOR_API UDefector_StateEffect : public UObject
{
	GENERATED_BODY()

public:

	virtual bool InitObject(APawn* Pawn);
	virtual void ExecuteEffect(float DeltaTime);
	virtual void DestroyObject();
};
