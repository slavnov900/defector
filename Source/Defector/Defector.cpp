// Copyright Epic Games, Inc. All Rights Reserved.

#include "Defector.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, Defector, "Defector" );

DEFINE_LOG_CATEGORY(LogDefector)
 