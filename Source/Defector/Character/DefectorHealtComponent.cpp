// Fill out your copyright notice in the Description page of Project Settings.


#include "DefectorHealtComponent.h"

// Sets default values for this component's properties
UDefectorHealtComponent::UDefectorHealtComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UDefectorHealtComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UDefectorHealtComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

float UDefectorHealtComponent::GetCurrentHealt()
{
	return Healt;
}

void UDefectorHealtComponent::SetCurrentHealt(float NewHealt)
{
	Healt = NewHealt;
}

void UDefectorHealtComponent::ChangeHealtValue(float ChangeValue)
{
	ChangeValue = ChangeValue * CoefDamage;
	Healt += ChangeValue;
	
	if(Healt > 100.0f)
	{
		Healt = 100.0f;
	}
	else
	{
		if(Healt < 0.0f)
		{
			OnDead.Broadcast();
			DeadEvent();
		}
	}
	onHealtChange.Broadcast(Healt, ChangeValue);
}

void UDefectorHealtComponent::DeadEvent_Implementation()
{
	//BP
}

