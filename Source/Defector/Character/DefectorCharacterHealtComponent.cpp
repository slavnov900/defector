// Fill out your copyright notice in the Description page of Project Settings.


#include "DefectorCharacterHealtComponent.h"
#include "Engine/World.h"

void UDefectorCharacterHealtComponent::ChangeHealtValue(float ChangeValue)
{
	

	float CurrentDamage = ChangeValue * CoefDamage;

	if(Shield > 0.0f && ChangeValue < 0.0f)
	{
		ChangeShieldValue(ChangeValue);
		
		if (Shield < 0.0f)
		{
			//FX
			UE_LOG(LogTemp, Warning, TEXT("UDefectorCharacterHealtComponent::ChangeHealtValue - Shield < 0"));
		}
	}
	else
	{
		Super::ChangeHealtValue(ChangeValue);
	}
	
}

float UDefectorCharacterHealtComponent::GetCurrentShield()
{
	return Shield;
}

void UDefectorCharacterHealtComponent::ChangeShieldValue(float ChangeValue)
{
	Shield += ChangeValue;
	
	if (Shield > 100.0f)
	{
		Shield = 100.f;
	}
	else
	{
		if(Shield < 0.0f)
			Shield = 0.0f;
	}

	if (GetWorld())
	{
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_CoolDownShieldTimer, this, &UDefectorCharacterHealtComponent::CoolDownShieldEnd, CoolDownShieldRecoverTime, false);

		GetWorld()->GetTimerManager().ClearTimer(TimerHandle_ShieldRecoveryRateTimer);
	}
	onShieldChange.Broadcast(Shield, ChangeValue);
}

void UDefectorCharacterHealtComponent::CoolDownShieldEnd()
{
	if (GetWorld())
	{
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_ShieldRecoveryRateTimer, this, &UDefectorCharacterHealtComponent::RecoveryShield, ShieldRecoveryRate, true);
	}
}

void UDefectorCharacterHealtComponent::RecoveryShield()
{
	float tmp = Shield;
	tmp = tmp + ShieldRecoveryValue;
	if (tmp > 100.0f)
	{
		Shield = 100.f;
		if (GetWorld())
		{
			GetWorld()->GetTimerManager().ClearTimer(TimerHandle_ShieldRecoveryRateTimer);
		}
	}
	else
	{
		Shield = tmp;
	}
	onShieldChange.Broadcast(Shield, ShieldRecoveryValue);
}
