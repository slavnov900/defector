// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "DefectorHealtComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FonHealtChange, float, Healt, float, Damage);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnDead);

USTRUCT(BlueprintType)
struct FStatsParam
{
	GENERATED_BODY()
	
};

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class DEFECTOR_API UDefectorHealtComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UDefectorHealtComponent();

	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Healt")
	FonHealtChange onHealtChange;
	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Healt")
	FOnDead OnDead;
	

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	float Healt = 100.f;

public:
	UPROPERTY(EditAnywhere, BluePrintReadWrite, Category = "Healt")
	float CoefDamage = 1.0f;
	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION(BlueprintCallable, Category= "Healt")
	float GetCurrentHealt();
	UFUNCTION(BlueprintCallable, Category= "Healt")
	void SetCurrentHealt(float NewHealt);
	UFUNCTION(BlueprintCallable, Category= "Healt")
	virtual void ChangeHealtValue(float ChangeValue);
	UFUNCTION(BlueprintNativeEvent)
	void DeadEvent();
		
};
