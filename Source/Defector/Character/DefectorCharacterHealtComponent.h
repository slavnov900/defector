// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "DefectorHealtComponent.h"
#include "DefectorCharacterHealtComponent.generated.h"

/**
 * 
 */
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FonShieldChange, float, Shield, float, Damage);

UCLASS()
class DEFECTOR_API UDefectorCharacterHealtComponent : public UDefectorHealtComponent
{
	GENERATED_BODY()

public:
	
	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Healt")
	FonShieldChange onShieldChange;

	FTimerHandle TimerHandle_CoolDownShieldTimer;
	FTimerHandle TimerHandle_ShieldRecoveryRateTimer;

protected:

	float Shield = 100.0f;

public:
	
	UPROPERTY(EditAnywhere, BluePrintReadWrite, Category = "Shield")
	float CoolDownShieldRecoverTime = 5.0f;
	UPROPERTY(EditAnywhere, BluePrintReadWrite, Category = "Shield")
	float ShieldRecoveryValue = 1.0f;
	UPROPERTY(EditAnywhere, BluePrintReadWrite, Category = "Shield")
	float ShieldRecoveryRate = 0.1f;
	
	void ChangeHealtValue(float ChangeValue) override;

	float GetCurrentShield();

	void ChangeShieldValue(float ChangeValue);

	void CoolDownShieldEnd();

	void RecoveryShield();
};
