// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Kismet/BlueprintFunctionLibrary.h" 
#include "Engine/DataTable.h"
//#include "Serialization/JsonTypes.h"
#include "Types.generated.h"

UENUM(BlueprintType)
enum class EMovementState : uint8
{ 
	Aim_State UMETA(DisplayName = "Aim State"),
	AimWalk_State UMETA(DisplayName = "AimWalk State"),
	Walk_State UMETA(DisplayName = "Walk State"),
	Run_State UMETA(DisplayName = "Run State"),
	SprintRun_State UMETA(DisplayName = "SprintRun State"),
};

UENUM(BlueprintType)
enum class EWeaponType : uint8
{
	RifleType UMETA(DisplayName = "Rifle"),
	ShotGunType UMETA(DisplayName = "ShotGun"),
	SniperRifle UMETA(DisplayName = "SniperRifle"),
	GrenadeLauncher UMETA(DisplayName = "GrenadeLauncher"),
	RocketLauncher UMETA(DisplayName = "RocketLauncher")
};


USTRUCT(BlueprintType)
struct FCharactedSpeed
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BluePrintReadWrite, Category = "Movement")
		float AimSpeedNormal = 300.0f;
	UPROPERTY(EditAnywhere, BluePrintReadWrite, Category = "Movement")
		float WalkSpeedNormal = 200.0f;
	UPROPERTY(EditAnywhere, BluePrintReadWrite, Category = "Movement")
		float RunSpeedNormal = 600.0f;
	UPROPERTY(EditAnywhere, BluePrintReadWrite, Category = "Movement")
		float AimSpeedWalk = 100.0f;
	UPROPERTY(EditAnywhere, BluePrintReadWrite, Category = "Movement")
		float SprintRunSpeedRun = 800.0f;
};

USTRUCT(BlueprintType)
struct FProjectileInfo
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BluePrintReadWrite, Category = "ProjectileSetting")
	TSubclassOf<class AProjectileDefault> Projectile = nullptr;
	UPROPERTY(EditAnywhere, BluePrintReadWrite, Category = "ProjectileSetting")
	UStaticMesh* ProjectileStaticMesh = nullptr;
	UPROPERTY(EditAnywhere, BluePrintReadWrite, Category = "ProjectileSetting")
	FTransform ProjectileStaticMeshOffset = FTransform();
	UPROPERTY(EditAnywhere, BluePrintReadWrite, Category = "ProjectileSetting")
	UParticleSystem* ProjectileTrailFX = nullptr;
	UPROPERTY(EditAnywhere, BluePrintReadWrite, Category = "ProjectileSetting")
	FTransform ProjectileTrailFxOffset = FTransform();
	
	UPROPERTY(EditAnywhere, BluePrintReadWrite, Category = "ProjectileSetting")
	float ProjectileDamage = 20.0f;
	UPROPERTY(EditAnywhere, BluePrintReadWrite, Category = "ProjectileSetting")
	float ProjectileLifeTime = 20.0f;
	UPROPERTY(EditAnywhere, BluePrintReadWrite, Category = "ProjectileSetting")
	float ProjectileInitSpeed = 2000.0f;

	//Material to decal on hit
	UPROPERTY(EditAnywhere, BluePrintReadWrite, Category = "FX")
	TMap<TEnumAsByte<EPhysicalSurface>, UMaterialInterface*> HitDecals;
	//Sound when hit
	UPROPERTY(EditAnywhere, BluePrintReadWrite, Category = "FX")
	USoundBase* HitSound = nullptr;
	//fx when hit check by surface
	UPROPERTY(EditAnywhere, BluePrintReadWrite, Category = "FX")
	TMap<TEnumAsByte<EPhysicalSurface>, UParticleSystem*> HitFXs;

	UPROPERTY(EditAnywhere, BluePrintReadWrite, Category = "ProjectileSetting")
	UParticleSystem* ExploseFX = nullptr;
	UPROPERTY(EditAnywhere, BluePrintReadWrite, Category = "ProjectileSetting")
	USoundBase* ExploseSound = nullptr;
	
	UPROPERTY(EditAnywhere, BluePrintReadWrite, Category = "ProjectileSetting")
		bool bIsLikeBomp = false;
	
	UPROPERTY(EditAnywhere, BluePrintReadWrite, Category = "Explode")
		UParticleSystem* ExplodeFX = nullptr;
	UPROPERTY(EditAnywhere, BluePrintReadWrite, Category = "Explode")
		USoundBase* ExplodeSound = nullptr;
	UPROPERTY(EditAnywhere, BluePrintReadWrite, Category = "Explode")
		float ProjectileMaxRadiusDamage = 200.0f;
	UPROPERTY(EditAnywhere, BluePrintReadWrite, Category = "Explode")
		float ProjectileMinRadiusDamage = 200.0f;
	UPROPERTY(EditAnywhere, BluePrintReadWrite, Category = "Explode")
		float ExploseMaxDamage = 40.0f;
	UPROPERTY(EditAnywhere, BluePrintReadWrite, Category = "Explode")
		float ExplodeFallOffCoef = 1.0f;
	
};

USTRUCT(BlueprintType)
struct FWeaponDispersion
{
	GENERATED_BODY()
	
	UPROPERTY(EditAnywhere, BluePrintReadWrite, Category = "Dispersion")
	float Aim_StateDispersionAimMax = 2.0f;
	UPROPERTY(EditAnywhere, BluePrintReadWrite, Category = "Dispersion")
	float Aim_StateDispersionAimMin = 0.3f;
	UPROPERTY(EditAnywhere, BluePrintReadWrite, Category = "Dispersion")
	float Aim_StateDispersionAimRecoil = 1.0f;
	UPROPERTY(EditAnywhere, BluePrintReadWrite, Category = "Dispersion")
	float Aim_StateDispersionAimReduction = 0.3f;

	UPROPERTY(EditAnywhere, BluePrintReadWrite, Category = "Dispersion")
	float AimWalk_StateDispersionAimMax = 1.0f;
	UPROPERTY(EditAnywhere, BluePrintReadWrite, Category = "Dispersion")
	float AimWalk_StateDispersionAimMin = 0.1f;
	UPROPERTY(EditAnywhere, BluePrintReadWrite, Category = "Dispersion")
	float AimWalk_StateDispersionAimRecoil = 1.0f;
	UPROPERTY(EditAnywhere, BluePrintReadWrite, Category = "Dispersion")
	float AimWalk_StateDispersionAimReduction = 0.4f;

	UPROPERTY(EditAnywhere, BluePrintReadWrite, Category = "Dispersion")
	float Walk_StateDispersionAimMax = 5.0f;
	UPROPERTY(EditAnywhere, BluePrintReadWrite, Category = "Dispersion")
	float Walk_StateDispersionAimMin = 1.0f;
	UPROPERTY(EditAnywhere, BluePrintReadWrite, Category = "Dispersion")
	float Walk_StateDispersionAimRecoil = 1.0f;
	UPROPERTY(EditAnywhere, BluePrintReadWrite, Category = "Dispersion")
	float Walk_StateDispersionAimReduction = 0.2f;

	UPROPERTY(EditAnywhere, BluePrintReadWrite, Category = "Dispersion")
	float Run_StateDispersionAimMax = 10.0f;
	UPROPERTY(EditAnywhere, BluePrintReadWrite, Category = "Dispersion")
	float Run_StateDispersionAimMin = 4.0f;
	UPROPERTY(EditAnywhere, BluePrintReadWrite, Category = "Dispersion")
	float Run_StateDispersionAimRecoil = 1.0f;
	UPROPERTY(EditAnywhere, BluePrintReadWrite, Category = "Dispersion")
	float Run_StateDispersionAimReduction = 0.1f;
};

USTRUCT(BlueprintType)
struct FAnimationWeaponInfo
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BluePrintReadWrite, Category = "AnimChar")
		UAnimMontage* AnimCharFire = nullptr;
	UPROPERTY(EditAnywhere, BluePrintReadWrite, Category = "AnimChar")
		UAnimMontage* AnimCharFireAim = nullptr;
	UPROPERTY(EditAnywhere, BluePrintReadWrite, Category = "AnimChar")
		UAnimMontage* AnimCharReload = nullptr;
	UPROPERTY(EditAnywhere, BluePrintReadWrite, Category = "AnimChar")
		UAnimMontage* AnimCharReloadAim = nullptr;
	UPROPERTY(EditAnywhere, BluePrintReadWrite, Category = "AnimWeapon")
		UAnimMontage* AnimWeaponReload = nullptr;
	UPROPERTY(EditAnywhere, BluePrintReadWrite, Category = "AnimWeapon")
		UAnimMontage* AnimWeaponReloadAim = nullptr;
	UPROPERTY(EditAnywhere, BluePrintReadWrite, Category = "AnimWeapon")
		UAnimMontage* AnimWeaponFire = nullptr;
};

USTRUCT(BlueprintType)
struct FDropMeshInfo
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BluePrintReadWrite, Category = "DropMesh")
		UStaticMesh* DropMesh = nullptr;
	UPROPERTY(EditAnywhere, BluePrintReadWrite, Category = "DropMesh")
		float DropMeshTime = -1.0f;
	UPROPERTY(EditAnywhere, BluePrintReadWrite, Category = "DropMesh")
		float DropMeshLifeTime = 10.0f;
	UPROPERTY(EditAnywhere, BluePrintReadWrite, Category = "DropMesh")
		FTransform DropMeshOffset = FTransform();
	UPROPERTY(EditAnywhere, BluePrintReadWrite, Category = "DropMesh")
		FVector DropMeshImpulseDir = FVector(0.0f);
	UPROPERTY(EditAnywhere, BluePrintReadWrite, Category = "DropMesh")
		float PowerImpulse = 0.0f;
	UPROPERTY(EditAnywhere, BluePrintReadWrite, Category = "DropMesh")
		float ImpulseRandomDispersion = 0.0f;
	UPROPERTY(EditAnywhere, BluePrintReadWrite, Category = "DropMesh")
		float CustomMass = 0.0f;

};

USTRUCT(BlueprintType)
struct FWeaponInfo : public FTableRowBase
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BluePrintReadWrite, Category = "Trace")
		float WeaponDamage = 20.0f;
	UPROPERTY(EditAnywhere, BluePrintReadWrite, Category = "State")
		float RateOfFire = 0.5f;
	
	UPROPERTY(EditAnywhere, BluePrintReadWrite, Category = "Class")
	TSubclassOf<class AWeaponDefault> WeaponClass = nullptr;
	
	UPROPERTY(EditAnywhere, BluePrintReadWrite, Category = "State")
	float ReloadTime = 2.0f;
	UPROPERTY(EditAnywhere, BluePrintReadWrite, Category = "State")
	int32 MaxRound = 10;
	UPROPERTY(EditAnywhere, BluePrintReadWrite, Category = "State")
	int32 NumberProjectileByShot = 1;

	UPROPERTY(EditAnywhere, BluePrintReadWrite, Category = "Dispersion")
	FWeaponDispersion DispersionWeapon;

	UPROPERTY(EditAnywhere, BluePrintReadWrite, Category = "Sound")
	USoundBase* SoundFireWeapon = nullptr;
	UPROPERTY(EditAnywhere, BluePrintReadWrite, Category = "Sound")
	USoundBase* SoundReloadWeapon = nullptr;
	UPROPERTY(EditAnywhere, BluePrintReadWrite, Category = "FX")
	UParticleSystem* EffectFireWeapon = nullptr;
	//if null use trace logic
	UPROPERTY(EditAnywhere, BluePrintReadWrite, Category = "Projectile")
	FProjectileInfo ProjectileSetting;
	UPROPERTY(EditAnywhere, BluePrintReadWrite, Category = "Trace")
	float DistanceTrace = 2000.0f;
	UPROPERTY(EditAnywhere, BluePrintReadWrite, Category = "HitEffect")
	UDecalComponent* DecalOnHit = nullptr;

	UPROPERTY(EditAnywhere, BluePrintReadWrite, Category = "Anim")
		FAnimationWeaponInfo AnimWeaponInfo;
	

	UPROPERTY(EditAnywhere, BluePrintReadWrite, Category = "Mesh")
		FDropMeshInfo ClipDropMesh;
	UPROPERTY(EditAnywhere, BluePrintReadWrite, Category = "Mesh")
		FDropMeshInfo ShellBullets;


	UPROPERTY(EditAnywhere, BluePrintReadWrite, Category = "Inventory")
		float SwitchTimeToWeapon = 1.0f;
	
	UPROPERTY(EditAnywhere, BluePrintReadWrite, Category = "Inventory")
	UTexture2D* WeaponIcon = nullptr;
	UPROPERTY(EditAnywhere, BluePrintReadWrite, Category = "Inventory")
	EWeaponType WeaponType = EWeaponType::RifleType;
};


USTRUCT(BlueprintType)
struct FAdditionalWeaponInfo
{
	GENERATED_BODY()
	UPROPERTY(EditAnywhere, BluePrintReadWrite, Category = "Weapon Stats")
	int32 Round = 10;
};

USTRUCT(BlueprintType)
struct FWeaponSlot
{
	GENERATED_BODY()
	
	UPROPERTY(EditAnywhere, BluePrintReadWrite, Category = "WeaponSlot")
	FName NameItem;
	UPROPERTY(EditAnywhere, BluePrintReadWrite, Category = "WeaponSlot")
	FAdditionalWeaponInfo AdditionalInfo;
};

USTRUCT(BlueprintType)
struct FAmmoSlot
{
	GENERATED_BODY()
	UPROPERTY(EditAnywhere, BluePrintReadWrite, Category = "AmmoSlot")
	EWeaponType WeaponType = EWeaponType::RifleType;
	UPROPERTY(EditAnywhere, BluePrintReadWrite, Category = "AmmoSlot")
	int32 Cout = 100;
	UPROPERTY(EditAnywhere, BluePrintReadWrite, Category = "AmmoSlot")
	int32 MaxCout = 100;
};

USTRUCT(BlueprintType)
struct FDropItem : public FTableRowBase
{
	GENERATED_BODY()
	UPROPERTY(EditAnywhere, BluePrintReadWrite, Category = "DropWeapon")
	UStaticMesh* WeaponStaticMesh = nullptr;
	UPROPERTY(EditAnywhere, BluePrintReadWrite, Category = "DropWeapon")
	USkeletalMesh* WeaponSkeletMesh = nullptr;
	UPROPERTY(EditAnywhere, BluePrintReadWrite, Category = "DropWeapon")
	FWeaponSlot WeaponInfo;
	UPROPERTY(EditAnywhere, BluePrintReadWrite, Category = "DropWeapon")
	UParticleSystem* ParticleItem = nullptr;
	UPROPERTY(EditAnywhere, BluePrintReadWrite, Category = "DropWeapon")
	FTransform Offset;
	
};	

UCLASS()
class DEFECTOR_API UTypes : public UBlueprintFunctionLibrary 
 {
	GENERATED_BODY()
};