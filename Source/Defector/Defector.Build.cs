// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class Defector : ModuleRules
{
	public Defector(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core",
            "CoreUObject",
            "Engine",
            "InputCore",
            "HeadMountedDisplay",
            "NavigationSystem",
            "AIModule" });
		PrivateDependencyModuleNames.AddRange(new string[] {"Slate", "SlateCore", "PhysicsCore"});

        PublicIncludePaths.AddRange(new string[]
        {
          "Defector/Character",
          "Defector/Game",
          "Defector/FuncLibrary",
          "Defector/interface"
        } );

    }



}
