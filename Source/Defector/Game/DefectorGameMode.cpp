// Copyright Epic Games, Inc. All Rights Reserved.

#include "DefectorGameMode.h"
#include "DefectorPlayerController.h"
#include "DefectorCharacter.h"
#include "UObject/ConstructorHelpers.h"

ADefectorGameMode::ADefectorGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = ADefectorPlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/BluePrint/Character/BP_Character"));
	if (PlayerPawnBPClass.Class != nullptr)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}

void ADefectorGameMode::PlayerCharacterDead()
{
	
}
