// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "DefectorGameMode.generated.h"

UCLASS(minimalapi)
class ADefectorGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ADefectorGameMode();

	void PlayerCharacterDead();
};



