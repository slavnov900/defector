// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Types.h"
#include "Engine/DataTable.h"
#include "Defector/WeaponDefault.h"
#include "Engine/GameInstance.h"
#include "DefectorGameInstance.generated.h"

UCLASS()
class DEFECTOR_API UDefectorGameInstance : public UGameInstance
{
	GENERATED_BODY()
	
public:	
	UPROPERTY(EditAnywhere, BluePrintReadWrite, Category = "WeaponSetting")
	UDataTable* WeaponInfoTable = nullptr;
	UPROPERTY(EditAnywhere, BluePrintReadWrite, Category = "WeaponSetting")
	UDataTable* DropItemInfoTable = nullptr;
	UFUNCTION(BlueprintCallable)
	bool GetWeaponInfoByName(FName NameWeapon, FWeaponInfo& OutInfo);
	UFUNCTION(BlueprintCallable)
	bool GetDropItemInfoByWeaponName(FName NameItem, FDropItem& OutInfo);
	UFUNCTION(BlueprintCallable)
	bool GetDropItemInfoByName(FName NameItem, FDropItem& OutInfo);
	
};
